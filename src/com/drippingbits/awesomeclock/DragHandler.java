package com.drippingbits.awesomeclock;
import java.awt.Point;
import java.awt.Window;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.peer.MouseInfoPeer;

import javax.swing.JFrame;


public class DragHandler implements MouseListener, MouseMotionListener,
		MouseInfoPeer {

	private int mouseX;
	private int mouseY;
	private JFrame frame;
	private ClockSettings settings;
	
	public DragHandler(JFrame frame, ClockSettings settings) {
		this.frame = frame;
		this.settings = settings;
	}
	
	@Override
	public void mouseReleased(MouseEvent arg0) {
		mouseX = -1;
		mouseY = -1;
		
		Point p = frame.getLocationOnScreen();
		settings.setValue(ClockSettings.X_VALUE, p.x);
		settings.setValue(ClockSettings.Y_VALUE, p.y);
		settings.save();
	}
	
	@Override
	public void mousePressed(MouseEvent me) {
		mouseX = me.getXOnScreen();
		mouseY = me.getYOnScreen();
	}
	
	@Override
	public void mouseExited(MouseEvent arg0) {
	}
	
	@Override
	public void mouseEntered(MouseEvent arg0) {
	}
	
	@Override
	public void mouseClicked(MouseEvent arg0) {
	}
	
	@Override
	public void mouseMoved(MouseEvent me) {
	}
	
	@Override
	public void mouseDragged(MouseEvent me) {
		if (mouseX > -1 && mouseY > -1) {
			
			int dx = me.getXOnScreen() - mouseX;
			int dy = me.getYOnScreen() - mouseY;
			
			mouseX = me.getXOnScreen();
			mouseY = me.getYOnScreen();
			
			frame.setLocation(frame.getX() + dx, frame.getY() + dy);
		}
	}

	@Override
	public int fillPointWithCoords(Point arg0) {
		return 0;
	}

	@Override
	public boolean isWindowUnderMouse(Window window) {
		return 	between((int) window.getMousePosition().x, window.getX(), window.getBounds().width) && 
				between((int) window.getMousePosition().y, window.getY(), window.getBounds().height);
	}
	
	private boolean between(int x, int min, int max) {
		return x >= min && x <= max;
	}

}
