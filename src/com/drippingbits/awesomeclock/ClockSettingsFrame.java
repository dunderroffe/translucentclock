package com.drippingbits.awesomeclock;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.filechooser.FileNameExtensionFilter;

public class ClockSettingsFrame extends JFrame  {

	private static final long serialVersionUID = 6383910550485545019L;
	
	public ClockSettingsFrame(final ClockSettings settings) {

		setLayout(new GridBagLayout());
		JButton hourColorBtn = createColorButton("Hour", ClockSettings.HOUR_COLOR, settings);
		JButton minuteColorBtn = createColorButton("Minute", ClockSettings.MINUTE_COLOR, settings);
		JButton secondColorBtn = createColorButton("Second", ClockSettings.SECOND_COLOR, settings);
		JButton ticsColorBtn = createColorButton("Tics", ClockSettings.TICS_COLOR, settings);
		JButton lableColorBtn = createColorButton("Lable", ClockSettings.LABLE_COLOR, settings);
		JButton borderColorBtn = createColorButton("Border", ClockSettings.BORDER_COLOR, settings);
		JButton backgroundColorBtn = createColorButton("Background", ClockSettings.BACKGROUND_COLOR, settings);
		
		JButton backgroundImageBtn = createImagePickerButton("Background Image", ClockSettings.BACKGROUND_IMAGE, settings);
		
		add(hourColorBtn);
		add(minuteColorBtn);
		add(secondColorBtn);
		add(ticsColorBtn);
		add(lableColorBtn);
		add(borderColorBtn);
		add(backgroundColorBtn);
		add(backgroundImageBtn);
		setResizable(false);
		pack();
	}
	private JButton createImagePickerButton(final String lable, final String imageId, final ClockSettings settings) {
		final JButton btn = new JButton();
		btn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent ae) {
				String path = null;
				if (settings.getImage(imageId) != null) {
					path = settings.getImage(imageId).getPath();
				}
				  JFileChooser chooser = new JFileChooser(path);
				    FileNameExtensionFilter filter = new FileNameExtensionFilter(
				        "jpg, gif and png", "jpg", "gif", "png");
				    chooser.setFileFilter(filter);
				    int returnVal = chooser.showOpenDialog(null);
				    if(returnVal == JFileChooser.APPROVE_OPTION) {
				    	settings.setImage(imageId, chooser.getSelectedFile().getPath());
				    } else if (returnVal == JFileChooser.CANCEL_OPTION) {
				    	settings.setImage(imageId, (RichImage) null);
				    }
			}
		});
		btn.setText(lable);
		return btn;
	}
	
	private JButton createColorButton(final String lable, final String colorId, final ClockSettings settings) {
		final JButton btn = new JButton();
		btn.setBackground(settings.getColor(colorId));
		btn.setText(lable);
		btn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent ae) {
				btn.setBackground(JColorChooser.showDialog(null, "Set " + lable + " color", btn.getBackground()));
				settings.setColor(colorId, btn.getBackground());
				settings.save();
			}
		});
		
		return btn;
	}
}
