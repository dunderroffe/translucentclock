package com.drippingbits.awesomeclock;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;

import static com.drippingbits.awesomeclock.ClockSettings.*;

public class ClockPanel extends JPanel {
	private static final long serialVersionUID = 8802512963023708747L;
	private static final double POINTER_BASE_WIDTH = 2;

	private static final int HOUR_BASE = 12;
	private static final int MINUTE_BASE = 60;
	private static final int SECOND_BASE = 60;
	private static final int MILLIS_BASE = 60 * 1000;

	private List<Line> ticLines;
	
	private ClockSettings settings;
	
	private JLabel time;
	private JLabel date;
	
	private SimpleDateFormat sdfTime;
	private SimpleDateFormat sdfDate;
	private Calendar cal;
	private float inner;
	private float outer;
	private float offsetX;
	private float offsetY;
	private float hourLength;
	private float minuteLength;
	private float millisLength;
	private boolean initialized;

	public ClockPanel(ClockSettings settings) {
		super(true);
		
		this.settings = settings;
		setOpaque(false);
		sdfTime = new SimpleDateFormat("HH:mm:ss");
		sdfDate = new SimpleDateFormat("dd MMM");
		
		setLayout(null);
		
		int width = 50;
		int height = 20;
		
		time = new JLabel();
		time.setBounds((int)(150 - (float)width/2),180 , width, height);
		
		date = new JLabel();
		date.setBounds((int)(150 - (float)width/2) + 6,200 , width, height);
		
		add(time);
		add(date);
	}

	@Override
	protected void paintComponent(Graphics g) {
		if (g instanceof Graphics2D) {
			if (!initialized) {
				init();
			}
			
			cal = Calendar.getInstance();
			
			time.setForeground(settings.getColor(LABLE_COLOR));
			time.setText(sdfTime.format(cal.getTime()));
			date.setForeground(settings.getColor(LABLE_COLOR));
			date.setText(sdfDate.format(cal.getTime()));
			
			Graphics2D g2d = (Graphics2D)g;

			renderAnalog(g2d);
		}
	}
	
	private void init() {
		offsetX = (float) getWidth() / 2;
		offsetY = (float) getHeight() / 2;

		float max = Math.min(offsetX, offsetY);
		inner = ((max/3) * 2);
		outer =  max;
		
		hourLength = (inner / 3) * 2;
		minuteLength = outer;
		millisLength = outer;		
		
		ticLines = new ArrayList<Line>();
		for (double angle = 0; angle < Math.PI * 2; angle += Math.PI/6) {
			double fx  =  Math.cos(angle);
			double fy  =  Math.sin(angle);
			
			int x1 = (int) (fx * inner + offsetX);
			int y1 = (int) ( fy * inner + offsetY);
			Point p1 = new Point(x1, y1);
			
			int x2 = (int) (fx * outer + offsetX);
			int y2 = (int) (fy * outer + offsetY);
			Point p2 = new Point(x2, y2);
			
			ticLines.add(new Line(p1, p2));
		}	
		initialized = true;
	}

	private void renderAnalog(Graphics2D g2d) {

		int hours = cal.get(Calendar.HOUR);
		int minutes = cal.get(Calendar.MINUTE);
		int seconds = cal.get(Calendar.SECOND);
		int millis = cal.get(Calendar.MILLISECOND);
	
		if (settings.getImage(BACKGROUND_IMAGE) != null) {
			g2d.drawImage(settings.getImage(ClockSettings.BACKGROUND_IMAGE).getImage(), 0, 0, getWidth(), getHeight(),null);
		} else {
			
			g2d.setColor(settings.getColor(BACKGROUND_COLOR));
			if (OsCheck.isUnix()) {
				g2d.fillRect(0, 0, getWidth(), getHeight());
			} else {
				g2d.fillOval(0, 0, getWidth(), getHeight());
			}
			
			g2d.setColor(settings.getColor(BORDER_COLOR));
			g2d.drawOval(0, 0, getWidth(), getHeight());
		}
		
		g2d.setColor(settings.getColor(TICS_COLOR));
		for (Line l: ticLines) {
			l.draw(g2d);
		}

		renderPointer(g2d, minutes + hours * 60, HOUR_BASE * MINUTE_BASE, settings.getColor(HOUR_COLOR), offsetX, offsetY, hourLength);
		renderPointer(g2d, seconds + minutes * 60, MINUTE_BASE * SECOND_BASE, settings.getColor(MINUTE_COLOR), offsetX, offsetY, minuteLength);
		renderPointer(g2d, (millis + seconds * 1000), MILLIS_BASE, settings.getColor(SECOND_COLOR), offsetX, offsetY, millisLength);
	}

	private void renderPointer (Graphics2D g2d, final double number, final double base, Color color, float offsetX, float offsetY, float length) {
				g2d.setColor(color);
		double angle = toAngle(number,base) * -1 + (Math.PI / 2);

				int[] xPoints = {
						(int) (offsetX + (Math.cos(angle + (Math.PI / 2)))*POINTER_BASE_WIDTH),
						(int) (offsetX + (Math.cos(angle - (Math.PI / 2)))*POINTER_BASE_WIDTH ),
						(int) (offsetX + Math.cos(angle) * length)
				};
		
				int[] yPoints = {
						getHeight() - (int) (offsetY + (Math.sin(angle + (Math.PI / 2)))*POINTER_BASE_WIDTH), 
						getHeight() - (int) (offsetY + (Math.sin(angle - (Math.PI / 2)))*POINTER_BASE_WIDTH), 
						getHeight() - (int) (offsetY + Math.sin(angle) * length)
				};
		
				g2d.fillPolygon(xPoints, yPoints, 3);
				g2d.drawPolygon(xPoints, yPoints, 3);
	}

	private double toAngle(final double number, final double base) {
		double fraction = (number/base);
		double ret = fraction * (Math.PI * 2);
		return ret;
	}
}
