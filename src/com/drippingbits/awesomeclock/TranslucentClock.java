package com.drippingbits.awesomeclock;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.Timer;

public class TranslucentClock extends JFrame {
	private static final long serialVersionUID = 7950951037044462579L;
	
	public TranslucentClock(int delay) throws Exception {
		super("AwesomeClock");
		final ClockSettings settings = new ClockSettings();

		DragHandler dh = new DragHandler(this, settings);

		addMouseListener(dh);
		addMouseMotionListener(dh);
		
		ClockPanel clock = new ClockPanel(settings);
		add(clock);

		addKeyListener(new KeyHandler(this, settings));

		setUndecorated(true);
		
		if (!OsCheck.isUnix()) {
		
			setBackground(new Color(0,0,0,0));
		}
		setSize(new Dimension(300,300));
		setAlwaysOnTop(true);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		Timer t = new Timer(delay, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent ae) {
				repaint();
			}
		});
		
		setVisible(true);
		t.start();
		
		if (settings.getValue(ClockSettings.X_VALUE) != null & settings.getValue(ClockSettings.Y_VALUE) != null) {
			setLocation(new Point(settings.getValue(ClockSettings.X_VALUE),settings.getValue(ClockSettings.Y_VALUE)));
		} else {
			Point p  = getLocationOnScreen();
			settings.setValue(ClockSettings.X_VALUE, p.x);
			settings.setValue(ClockSettings.Y_VALUE, p.y);;
		}
	}


}