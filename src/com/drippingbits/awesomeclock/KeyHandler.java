package com.drippingbits.awesomeclock;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

public class KeyHandler implements KeyListener {
	private ClockSettings settings;
	private JFrame frame;
	
	public KeyHandler(JFrame frame, ClockSettings settings) {
		this.frame = frame;
		this.settings = settings;
	}
	
	@Override
	public void keyTyped(KeyEvent ke) {
	}

	@Override
	public void keyReleased(KeyEvent ke) {
		if (ke.getKeyCode() == KeyEvent.VK_ESCAPE) {
			settings.save();
			frame.dispose();
			System.exit(0);
		}

		if (ke.getKeyCode() == KeyEvent.VK_F1) {
			ClockSettingsFrame csf = new ClockSettingsFrame(settings);
			csf.setVisible(true);
		}
	}

	@Override
	public void keyPressed(KeyEvent ke) {
	}
}