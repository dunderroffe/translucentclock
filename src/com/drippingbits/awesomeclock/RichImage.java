package com.drippingbits.awesomeclock;

import java.awt.Image;

public class RichImage {
	private Image image;
	private String path;
	
	public RichImage(Image image, String path) {
		this.image = image;
		this.path = path;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
}
