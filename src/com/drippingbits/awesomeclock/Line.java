package com.drippingbits.awesomeclock;

import java.awt.Graphics2D;
import java.awt.Point;

public class Line {
	Point p1;
	Point p2;
	
	public Line(Point p1, Point p2) {
		this.p1 = p1;
		this.p2 = p2;
	}
	
	public void draw(Graphics2D g2d) {
		g2d.drawLine(p1.x, p1.y, p2.x, p2.y);
	}
}
