package com.drippingbits.awesomeclock;
import static java.awt.GraphicsDevice.WindowTranslucency.PERPIXEL_TRANSLUCENT;

import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;

import javax.swing.SwingUtilities;


public class StartupSequence {
	public static void main(String[] args) {
		// Determine what the GraphicsDevice can support.
		GraphicsEnvironment ge = 
				GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice gd = ge.getDefaultScreenDevice();
		boolean isPerPixelTranslucencySupported = 
				gd.isWindowTranslucencySupported(PERPIXEL_TRANSLUCENT);

		//If translucent windows aren't supported, exit.
		if (!isPerPixelTranslucencySupported) {
			System.err.println(
					"Per-pixel translucency is not supported");
			System.exit(0);
		}


		// Reads arguments as input
		final int delay;
		if (args.length > 0) {
			try {
				delay = Integer.parseInt(args[0]);
			} catch (NumberFormatException e) {
				throw new IllegalArgumentException("Cannot parse first argument '" + args[0] + "' to int", e);
			}
		} else {
			delay = 50;
		}

		// Create the GUI on the event-dispatching thread
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					new TranslucentClock(delay);
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});

	}
}
