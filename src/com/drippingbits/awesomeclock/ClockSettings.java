package com.drippingbits.awesomeclock;
import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Scanner;

import javax.swing.ImageIcon;

public class ClockSettings {

	private static final String COLORS = "COLORS";
	public static final String HOUR_COLOR = "HOUR_COLOR";
	public static final String MINUTE_COLOR = "MINUTE_COLOR";
	public static final String SECOND_COLOR = "SECOND_COLOR";
	public static final String TICS_COLOR = "TICS_COLOR";
	public static final String LABLE_COLOR = "LABLE_COLOR";
	public static final String BACKGROUND_COLOR = "BACKGROUND_COLOR";
	public static final String BORDER_COLOR = "BORDER_COLOR";
	public static final String LOCATION = "LOCATION";

	private static final String IMAGES = "IMAGES";
	public static final String BACKGROUND_IMAGE = "BACKGROUND_IMAGE";

	private static final String VALUES = "VALUES";
	public static final String X_VALUE = "X_VALUE";
	public static final String Y_VALUE = "Y_VALUE";
	public static final String WIDTH_VALUE = "WIDTH_VALUE";
	public static final String HEIGHT_VALUE = "HEIGHT_VALUE";
	public static final String HOUR_LENGHT_VALUE = "HOUR_LENGHT_VALUE";
	public static final String SECOND_LENGHT_VALUE = "SECOND_LENGHT_VALUE";
	public static final String MINUTE_LENGHT_VALUE = "MINUTE_LENGHT_VALUE";

	private final File saveFile;

	private final HashMap<String,Color> colorMap;
	private final HashMap<String, RichImage> imageMap;
	private final HashMap<String, Integer> valuesMap;

	public ClockSettings() throws FileNotFoundException, Exception {
		colorMap = new HashMap<String, Color>();
		imageMap = new HashMap<String, RichImage>();
		valuesMap = new HashMap<String, Integer>();
		String userHome = System.getProperty("user.home");

		if (OsCheck.isUnix()) {
			saveFile = new File(userHome + "/.awesome_clock/.settings");
		} else if (OsCheck.isWindows()) {
			saveFile = new File(userHome + "/AppData/Roaming/awesome_clock/settings");
		} else if (OsCheck.isMac()) {
			saveFile = new File(userHome + "/Library/Preferences/awesome_clock/.settings");
		} else {
			saveFile = new File(userHome + "/awesome_clock/.settings");
		}

		File file = saveFile;

		if (file == null) {
			throw new Exception("File cannot be null");
		}

		if (!file.exists()) {
			makeNew(file);

		} else {

			Scanner s = new Scanner(file);
			LoadCode lc = null;
			while (s.hasNext()) {
				String line = s.nextLine();
				switch (line) {
				case COLORS: 
					lc = new LoadCode(){
						@Override
						public void execute(String id, String value) {
							setColor(id, colorFromString(value));
						}
					};
					break;
				case IMAGES:
					lc = new LoadCode(){
						@Override
						public void execute(String id, String value) {
							setImage(id, value);
						}
					};
					break;
				case VALUES:
					lc = new LoadCode(){
						@Override
						public void execute(String id, String value) {
							setValue(id, Integer.parseInt(value));
						}
					};
					break;
				default:
					if (line.charAt(0) == '\t') {
						load(line, lc);
					} else if(line.charAt(0) != '#') {
						throw new Exception("Malformed settings line " + line);
					}
				}

			}
			s.close();
		}
	}

	private void makeNew(File file) throws IOException {	
		file.getParentFile().mkdirs();
		file.createNewFile();

		colorMap.put(HOUR_COLOR, Color.DARK_GRAY);
		colorMap.put(MINUTE_COLOR, Color.GRAY);
		colorMap.put(SECOND_COLOR, Color.RED);
		colorMap.put(TICS_COLOR, Color.LIGHT_GRAY);
		colorMap.put(LABLE_COLOR, Color.DARK_GRAY);
		colorMap.put(BACKGROUND_COLOR, new Color(0,0,0,0));

		save();		
	}

	private void load(String line, LoadCode lc) throws Exception {
		String[] split= line.trim().split("=");
		if (split.length == 2) {
			lc.execute(split[0], split[1]);
		}
		else {
			throw new Exception("Malformed settings line " + line);
		}
	}


	public synchronized Color getColor(String id) {
		return colorMap.get(id);
	}

	public synchronized void setColor(String id, Color color) {
		colorMap.put(id, color);
	}

	public synchronized RichImage getImage(String id) {
		return imageMap.get(id);
	}

	public synchronized void setImage(String id, RichImage image) {
		RichImage throwAway = imageMap.put(id, image);

		if (throwAway != null) {
			throwAway.setPath(null);
			throwAway.getImage().flush();
			throwAway.setImage(null);
		}
	}

	public synchronized void setImage(String imageId, String path) {
		setImage(imageId, new RichImage(new ImageIcon(path).getImage(), path));
	}

	public synchronized Integer getValue(String id) {
		return valuesMap.get(id);
	}

	public synchronized void setValue(String id, Integer value) {
		valuesMap.put(id, value);
	}

	public void save() {
		try {
			PrintWriter pw = new PrintWriter(saveFile);
			writeComment(pw,"This file handles settings for AwesomeClock.");
			writeComment(pw,"Rows that start with # are regarded as a comment, ");
			writeComment(pw,"but keep in mind that this file will be rewritten each time the application is saved!");

			pw.println(COLORS);
			for (Entry<String, Color> e : colorMap.entrySet()) {
				writeValue(pw, e.getKey(), colorToString(e.getValue()));
			}

			pw.println(IMAGES);
			for (Entry<String, RichImage> e : imageMap.entrySet()) {
				if (e.getValue() != null && e.getKey() != null) {
					writeValue(pw, e.getKey(), e.getValue().getPath());
				}
			}

			pw.println(VALUES);
			for (Entry<String, Integer> e : valuesMap.entrySet()) {
				writeValue(pw, e.getKey(), e.getValue() + "");
			}

			pw.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
	}

	private void writeComment(PrintWriter pw, String comment) {
		pw.println("#"+comment);
	}

	private void writeValue(PrintWriter pw, String id, String value) {
		pw.println('\t'+id + "=" + value);
	}

	private String colorToString(Color color) {
		return color.getRed()+":"+color.getGreen()+":"+color.getBlue()+":"+color.getAlpha();
	}

	private Color colorFromString(String str) {
		String[] split = str.split(":");

		switch (split.length) {
		case 3: return new Color(
				Integer.parseInt(split[0]), 
				Integer.parseInt(split[1]),
				Integer.parseInt(split[2]));
		case 4: return new Color(
				Integer.parseInt(split[0]), 
				Integer.parseInt(split[1]),
				Integer.parseInt(split[2]),
				Integer.parseInt(split[3]));
		default: throw new IllegalArgumentException("Bad inputstring");
		}
	}

	private interface LoadCode {

		void execute(String id, String value);
	}


}
