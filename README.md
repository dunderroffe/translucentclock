About
=====
This is a translucent clock that resides on the desktop.
I created it in order to be able to see what time it is on my second monitor (Windows 7).
Since I did not find any clock widget that suited my needs, I decided to make my own.

Usage
=====
Default update interval is 50 milliseconds it can be changed by giving an alternative update interval as argument 0

Press F1 to edit settings.

Press ESC to exit the application.

Click and drag to move the clock.

Known bugs
==========
Tranclusancy does not work on all display managers in Linux.
